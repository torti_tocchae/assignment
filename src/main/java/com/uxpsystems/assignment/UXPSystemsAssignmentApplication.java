package com.uxpsystems.assignment;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.ApplicationContext;

/**
 * Spring Boot Application Runner
 * @author Torti Ama-Njoku @ UXP
 */
@SpringBootApplication
public class UXPSystemsAssignmentApplication extends SpringBootServletInitializer {
    
    /**
     * Logger for the class
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(UXPSystemsAssignmentApplication.class);

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(UXPSystemsAssignmentApplication.class);
    }

    /**
     * Runtime method executed when application is started
     * @param args command line arguments
     */
    public static void main(String[] args) {
        ApplicationContext ctx = SpringApplication.run(UXPSystemsAssignmentApplication.class, args);

        String[] beanNames = ctx.getBeanDefinitionNames();
        Arrays.sort(beanNames);

        StringBuilder sb = new StringBuilder("Application beans:\n");
        for (String beanName : beanNames) {
            sb.append(beanName).append("\n");
        }
        LOGGER.info(sb.toString());
    }
}
