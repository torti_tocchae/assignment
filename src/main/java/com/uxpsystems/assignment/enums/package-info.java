/**
 * This package contains all enums used in the application.
 * 
 * @since 1.0
 * @author Torti Ama-Njoku @ UXP
 * @version 1.0
 */
package com.uxpsystems.assignment.enums;
