package com.uxpsystems.assignment.enums;

/**
 * Enum class to hold user statuses
 * 
 * @author Torti Ama-Njoku @ UXP
 */
public enum UserStatusEnum {
    
    /**
     * Active user status
     */
    ACTIVATED("Activated"),
    /**
     * An inactive user status
     */
    DEACTIVATED("Deactivated");

    private final String labelKey;
    
    /**
     * Initialisation
     *
     * @param labelKey replacement string
     */
    UserStatusEnum(String labelKey) {
            this.labelKey = labelKey;
    }
    
    /**
     * UserStatusEnum String representation
     *
     * @return String
     */
    public String getKeyValue() {
            return labelKey;
    }
}
