package com.uxpsystems.assignment.enums;

import org.springframework.http.HttpStatus;

/**
 * Holds all response codes, both errors and successful
 * 
 * @author Torti Ama-Njoku @ UXP
 */
public enum ResponseEnum {
    
    /**
     * When a request is successful
     */
    SUCCESSFUL("00", "request.successful", HttpStatus.OK),
    /**
     * When a request is successful
     */
    CONTENT_CREATED("00", "request.successful", HttpStatus.CREATED),
    /**
     * When a request is partially successful
     */
    PARTIALLY_SUCCESSFUL("01", "request.partiallySuccessful", HttpStatus.OK),
    /**
     * For an unexpected error
     */
    RESPONSE_CODE_100("100", "unexpectedError", HttpStatus.INTERNAL_SERVER_ERROR),
    /**
     * For when rest parameters fail validation
     */
    RESPONSE_CODE_101("101", "method.argument.not.valid", HttpStatus.BAD_REQUEST),
    
    //-------User responses - 110 - 119
    /**
     * When a user with a given ID is not found
     */
    RESPONSE_CODE_110("110", "user.notFound.id", HttpStatus.NO_CONTENT),
    /**
     * When a user with a given phone number is not found
     */
    RESPONSE_CODE_111("111", "user.notFound.username", HttpStatus.NO_CONTENT),
    /**
     * When user's mandatory details are missing on save
     */
    RESPONSE_CODE_112("112", "user.save.mandatoryDetails", HttpStatus.PARTIAL_CONTENT),
    /**
     * If a duplicate user already exists
     */
    RESPONSE_CODE_113("113", "user.save.duplicate", HttpStatus.CONFLICT),
    /**
     * If a password and password retype do not match
     */
    RESPONSE_CODE_114("114", "user.signup.passwordMismatch", HttpStatus.BAD_REQUEST);

    private final String code;
    private final String labelKey;
    private final HttpStatus httpCode;
    
    /**
     * @param code error code
     * @param labelKey error display label key
     * @param httpCode error http code
     */
    ResponseEnum(String code, String labelKey, HttpStatus httpCode) {
        this.code = code;
        this.labelKey = labelKey;
        this.httpCode = httpCode;
    }
    
    /**
     * @return the error code
     */
    public String getCode() {
        return code;
    }

    /**
     * @return the error display label key
     */
    public String getLabelKey() {
        return labelKey;
    }

    /**
     * @return the returned HTTP code
     */
    public HttpStatus getHttpCode() {
        return httpCode;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("ResponseEnum");
        sb.append("{code='").append(code).append('\'');
        sb.append(", labelKey='").append(labelKey).append('\'');
        sb.append(", httpCode='").append(httpCode).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
