package com.uxpsystems.assignment.data.model;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * This class describes attributes used to search the user entity. The singular attribute
 * names must match the corresponding attribute names in the related entity class.
 * 
 * @author Torti Ama-Njoku @ UXP
 */
@StaticMetamodel(User.class)
public final class User_ {
    public static volatile SingularAttribute<User, Long> id;
    public static volatile SingularAttribute<User, String> username;
}
