package com.uxpsystems.assignment.data.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.uxpsystems.assignment.enums.UserStatusEnum;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import org.apache.commons.lang3.StringUtils;

import org.apache.commons.lang3.builder.CompareToBuilder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * This class is used to represent available users on the database
 *
 * @author Torti Ama-Njoku @ UXP
 */
@Entity
@Table(name = "uxp_user", uniqueConstraints = {
    @UniqueConstraint(name = "uq_usr_username", columnNames = {"username"})})
public class User implements Serializable, Comparable<User> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "status", nullable = false, length = 20)
    @Enumerated(EnumType.STRING)
    private UserStatusEnum status;

    @Column(name = "username", length = 20, nullable = false)
    private String username;

    @Column(name = "password", length = 100, nullable = false)
    @JsonIgnore
    private String password;
    
    /**
     * Default constructor - creates a new instance with no values set.
     */
    public User() {
        super();
    }
    
    /**
     * Create User with all applicable attributes
     * 
     * @param username username
     * @param password login password
     */
    public User(String username, String password) {
        this.status = UserStatusEnum.ACTIVATED;
        this.username = username;
        this.password = password;
    }
    
    /**
     * Create a user object with all the attributes
     * @param id primary identifier
     * @param status user's status {@link UserStatusEnum}
     * @param username unique identifier
     * @param password 
     */
    public User(Long id, UserStatusEnum status, String username, String password) {
        this.id = id;
        this.status = status;
        this.username = username;
        this.password = password;
    }
    
    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the status
     */
    public UserStatusEnum getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(UserStatusEnum status) {
        this.status = status;
    }
    
    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }
    
    /**
     * Check if this user has been saved to the DB already
     * @return true if it has and false if not
     */
    @Transient
    @JsonIgnore
    public boolean isPersisted() {
        return getId() != null;
    }
    
    /**
     * Check if there are any missing mandatory details in the user object
     * @return true if there are any missing mandatory details and false if not
     */
    public boolean hasMissingDetails() {
        return StringUtils.isAnyBlank(getUsername(), getPassword()) || getStatus() == null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        User rhs = (User) obj;
        return new EqualsBuilder().append(getUsername(), rhs.getUsername()).isEquals();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        // you pick a hard-coded, randomly chosen, non-zero, odd number
        // ideally different for each class
        return new HashCodeBuilder(11, 31).append(getUsername()).toHashCode();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SIMPLE_STYLE).build();
    }

    @Override
    public int compareTo(User o) {
        return new CompareToBuilder().append(this.getId(), o.getId()).toComparison();
    }

}
