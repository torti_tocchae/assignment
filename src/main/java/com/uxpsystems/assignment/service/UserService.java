package com.uxpsystems.assignment.service;

import com.uxpsystems.assignment.data.model.User;
import com.uxpsystems.assignment.enums.UserStatusEnum;
import com.uxpsystems.assignment.exception.InvalidParameterException;
import com.uxpsystems.assignment.exception.AlreadyExistsException;
import com.uxpsystems.assignment.exception.MandatoryDetailsException;
import com.uxpsystems.assignment.exception.UserNotFoundException;
import java.util.List;

/**
 * Interface service class for executing desired {@link User} object operations
 * @author Torti Ama-Njoku @ UXP
 */
public interface UserService {
    
    /**
     * Get a list of all users
     * @return retrieved list of User objects
     */
    List<User> getAllUsers();
    
    /**
     * Find a user by the user ID, i.e. the primary identifier in the table
     * @param id primary identifier value of the user to be found
     * @return retrieved User object
     * @throws UserNotFoundException if the user is not found
     */
    User findById(Long id) throws UserNotFoundException;

    /**
     * Find a user by the user's unique username
     * @param username username of the user to be searched
     * @return retrieved User object
     * @throws UserNotFoundException if the user is not found
     */
    User findUser(String username) throws UserNotFoundException;
    
    /**
     * Delete a user from the database
     * @param id primary identifier value of the user to be deleted
     * @throws UserNotFoundException if the user is not found
     */
    void deleteUser(Long id) throws UserNotFoundException;
    
    /**
     * Save a new user to the DB
     * @param user user to be saved
     * @return saved user object
     * @throws AlreadyExistsException if the phone number and email 
     *         attempting to be saved already exists for another user
     * @throws MandatoryDetailsException if some mandatory details are missing
     */
    User saveUser(User user) throws AlreadyExistsException,  MandatoryDetailsException;
    
    /**
     * Update a user's details
     * @param userToUpdate user to be updated
     * @param username new username
     * @param password new password
     * @param status new status
     * @return saved user object
     * @throws AlreadyExistsException if the phone number and email 
     *         attempting to be saved already exists for another user
     */
    User updateUser(User userToUpdate, String username, String password, UserStatusEnum status) 
            throws AlreadyExistsException;
    
    /**
     * Register a new user
     * @param username unique username for this user
     * @param password password for the user's account
     * @param passwordRetype password confirmation retype, must be identical to password
     * @return the created user object
     * @throws AlreadyExistsException
     * @throws MandatoryDetailsException 
     * @throws InvalidParameterException
     */
    User signUp(String username, String password, String passwordRetype) 
            throws AlreadyExistsException, MandatoryDetailsException, InvalidParameterException;
}