package com.uxpsystems.assignment.service.impl;

import com.uxpsystems.assignment.dao.specification.UserSpecifications;
import com.uxpsystems.assignment.dao.UserDao;
import com.uxpsystems.assignment.data.model.User;
import com.uxpsystems.assignment.enums.ResponseEnum;
import com.uxpsystems.assignment.enums.UserStatusEnum;
import com.uxpsystems.assignment.exception.InvalidParameterException;
import com.uxpsystems.assignment.exception.AlreadyExistsException;
import com.uxpsystems.assignment.exception.MandatoryDetailsException;
import com.uxpsystems.assignment.exception.UserNotFoundException;
import java.util.Locale;


import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.uxpsystems.assignment.service.UserService;
import java.util.List;
import org.springframework.data.jpa.domain.Specification;

/**
 * Implementation of UserService interface.
 * 
 * @author Torti Ama-Njoku @ UXP
 */
@Service("userService")
public class UserServiceImpl implements UserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

    private final PasswordEncoder passwordEncoder;

    private final UserDao userDao;

    private final MessageSource messageSource;
    
    /**
     * Used by spring to instantiate a bean of this class autowiring by argument
     * resolvers the parameters in this constructor.
     * 
     * @param passwordEncoder used to encode the password
     * @param userDao 
     * @param messageSource source for error messages
     */
    public UserServiceImpl(final PasswordEncoder passwordEncoder, final UserDao userDao, MessageSource messageSource) {
        this.passwordEncoder = passwordEncoder;
        this.userDao = userDao;
        this.messageSource = messageSource;
    }
    
    @Transactional(readOnly = true)
    @Override
    public List<User> getAllUsers() {
        List<User> found = userDao.findAll();
        return found;
    }
    
    @Transactional(readOnly = true)
    @Override
    public User findById(Long id) throws UserNotFoundException {
        User found = userDao.findOne(id);
        if (found == null) {
            throw new UserNotFoundException(
                //user.notFound.id
                messageSource.getMessage(ResponseEnum.RESPONSE_CODE_110.getLabelKey(), 
                        new Object[] {id}, Locale.CANADA),
                ResponseEnum.RESPONSE_CODE_110);
        }
        return found;
    }

    @Transactional(readOnly = true)
    @Override
    public User findUser(String username) throws UserNotFoundException {
        Specification<User> searchSpec = UserSpecifications.searchByUsername(username);
        User found = userDao.findOne(searchSpec);
        if (found == null) {
            throw new UserNotFoundException(
                //user.notFound.username
                messageSource.getMessage(ResponseEnum.RESPONSE_CODE_111.getLabelKey(), 
                        new Object[] {username}, Locale.CANADA),
                ResponseEnum.RESPONSE_CODE_111);
        }
        return found;
    }
    
    /**
     * Checks if a user already exists
     * @param username username to check for
     * @return true if the user exists or false if not
     */
    private boolean userExists(String username) {
        try {
            findUser(username);
            return true;
        } catch (UserNotFoundException ex) {
            return false;
        }
    }
    
    @Transactional
    @Override
    public void deleteUser(Long id) throws UserNotFoundException {
        User deleted = findById(id);
        userDao.delete(deleted);
    }

    @Transactional
    @Override
    public User saveUser(User user) throws AlreadyExistsException, MandatoryDetailsException {

        // Check if there is any missing mandatory details, throw the error
        if (user.hasMissingDetails()) {
            throw new MandatoryDetailsException(
                //user.save.mandatoryDetails
                messageSource.getMessage(ResponseEnum.RESPONSE_CODE_112.getLabelKey(), 
                        null, Locale.CANADA),
                ResponseEnum.RESPONSE_CODE_112);
        }
        
        // Check if the unique details is applicable to another user already
        if (userExists(user.getUsername())) {
            throw new AlreadyExistsException(
                //user.save.duplicate
                messageSource.getMessage(ResponseEnum.RESPONSE_CODE_113.getLabelKey(), 
                        null, Locale.CANADA),
                ResponseEnum.RESPONSE_CODE_113);
        }

        // Encode the password
        if (passwordEncoder != null) {
            // Check whether we have to encrypt (or re-encrypt) the password
            user.setPassword(passwordEncoder.encode(user.getPassword()));
        } else {
            LOGGER.warn("PasswordEncoder not set, skipping password encryption...");
        }
        
        // Save the new user or user changes to the DB
        User savedUser = userDao.saveAndFlush(user);
        
        // Return the new user object
        return savedUser;
    }
    
    @Transactional
    @Override
    public User updateUser(User userToUpdate, String username, String password, UserStatusEnum status) 
            throws AlreadyExistsException {
        
        // Check if the unique details is applicable to another user already
        if (StringUtils.isNotBlank(username) 
                && !StringUtils.equalsIgnoreCase(userToUpdate.getUsername(), username) 
                && userExists(username)) {
            throw new AlreadyExistsException(
                //user.save.duplicate
                messageSource.getMessage(ResponseEnum.RESPONSE_CODE_113.getLabelKey(), 
                        null, Locale.CANADA),
                ResponseEnum.RESPONSE_CODE_113);
        }
        
        // Update the user with the new details
        if (StringUtils.isNotBlank(username)) {
            userToUpdate.setUsername(username);
        }

        if (StringUtils.isNotBlank(password)) {
            userToUpdate.setPassword(passwordEncoder.encode(password));
        }

        if (status != null) {
            userToUpdate.setStatus(status);
        }
        
        // Save the new user or user changes to the DB
        User savedUser = userDao.saveAndFlush(userToUpdate);
        
        // Return the new user object
        return savedUser;
    }

    @Transactional
    @Override
    public User signUp(String username, String password, String passwordRetype) 
            throws AlreadyExistsException, MandatoryDetailsException, InvalidParameterException {
        
        // Validate the password input
        if (!StringUtils.equals(password, passwordRetype)) {
            throw new InvalidParameterException(
                //user.signup.passwordMismatch
                messageSource.getMessage(ResponseEnum.RESPONSE_CODE_114.getLabelKey(), 
                        null, Locale.CANADA),
                ResponseEnum.RESPONSE_CODE_114);
        }
        
        // Setup the user object to be saved
        User user = new User(username, password);
        
        // Save the user
        return saveUser(user);
    }

}