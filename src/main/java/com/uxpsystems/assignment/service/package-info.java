/**
 * This package contains service interfaces used to access/perform operations on the data.
 * 
 * @since 1.0
 * @author Torti Ama-Njoku @ UXP
 * @version 1.0
 */
package com.uxpsystems.assignment.service;
