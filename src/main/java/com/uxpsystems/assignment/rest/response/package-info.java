/**
 * This package contains response POJO classes.
 * 
 * @since 1.0
 * @author Torti Ama-Njoku @ Tocchae
 * @version 1.0
 */
package com.uxpsystems.assignment.rest.response;
