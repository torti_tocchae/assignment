package com.uxpsystems.assignment.rest.response;

import com.uxpsystems.assignment.data.model.User;
import java.util.List;

/**
 * REST webservice response entity for list of users
 *
 * @author Torti Ama-Njoku @ UXP
 */
public class UserListDataHolderResponse extends GenericWSDataHolderResponse {

    private List<User> users;
    
    /**
     * Default Constructor
     */
    public UserListDataHolderResponse() {
        super();
    }
    
    /**
     * @param responseCode webservice response code
     * @param responseMessage webservice response message
     * @param status HTTP status code
     * @param timeStamp time stamp of the reponse
     */
    public UserListDataHolderResponse(String responseCode, String responseMessage, 
            int status, long timeStamp) {
        super(responseCode, responseMessage, status, timeStamp);
    }

    /**
     * @param responseCode webservice response code
     * @param responseMessage webservice response message
     * @param status HTTP status code
     * @param timeStamp time stamp of the reponse
     * @param users list of users to be returned
     */
    public UserListDataHolderResponse(String responseCode, String responseMessage, 
            int status, long timeStamp, List<User> users) {
        super(responseCode, responseMessage, status, timeStamp);
        this.users = users;
    }

    /**
     * @return the users
     */
    public List<User> getUsers() {
        return users;
    }

    /**
     * @param users the users to set
     */
    public void setUser(List<User> users) {
        this.users = users;
    }
}
