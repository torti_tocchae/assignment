package com.uxpsystems.assignment.rest.response;

import com.uxpsystems.assignment.data.model.User;

/**
 * REST webservice response entity for user details
 *
 * @author Torti Ama-Njoku @ UXP
 */
public class UserDataHolderResponse extends GenericWSDataHolderResponse {

    private User user;
    
    /**
     * Default Constructor
     */
    public UserDataHolderResponse() {
        super();
    }
    
    /**
     * @param responseCode webservice response code
     * @param responseMessage webservice response message
     * @param status HTTP status code
     * @param timeStamp time stamp of the reponse
     */
    public UserDataHolderResponse(String responseCode, String responseMessage, 
            int status, long timeStamp) {
        super(responseCode, responseMessage, status, timeStamp);
    }

    /**
     * @param responseCode webservice response code
     * @param responseMessage webservice response message
     * @param status HTTP status code
     * @param timeStamp time stamp of the reponse
     * @param user to be returned
     */
    public UserDataHolderResponse(String responseCode, String responseMessage, 
            int status, long timeStamp, User user) {
        super(responseCode, responseMessage, status, timeStamp);
        this.user = user;
    }

    /**
     * @return the user
     */
    public User getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(User user) {
        this.user = user;
    }
}
