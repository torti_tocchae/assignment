/**
 * This package contains REST exception handlers.
 * 
 * @since 1.0
 * @author Torti Ama-Njoku @ Tocchae
 * @version 1.0
 */
package com.uxpsystems.assignment.rest.exceptionhandler;
