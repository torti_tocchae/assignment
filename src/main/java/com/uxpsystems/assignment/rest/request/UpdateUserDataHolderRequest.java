package com.uxpsystems.assignment.rest.request;

import com.uxpsystems.assignment.enums.UserStatusEnum;

/**
 * REST webservice request entity for user updates
 * @author Torti Ama-Njoku @ UXP
 */
public class UpdateUserDataHolderRequest {

    private String username;

    private String newPassword;
    
    private UserStatusEnum status;
    
    /**
     * Default Constructor
     */
    public UpdateUserDataHolderRequest() {
        super();
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the newPassword
     */
    public String getNewPassword() {
        return newPassword;
    }

    /**
     * @param newPassword the newPassword to set
     */
    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    /**
     * @return the status
     */
    public UserStatusEnum getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(UserStatusEnum status) {
        this.status = status;
    }
}
