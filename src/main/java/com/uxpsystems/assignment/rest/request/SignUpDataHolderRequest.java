package com.uxpsystems.assignment.rest.request;

import javax.validation.constraints.NotNull;

/**
 * REST webservice request entity for user sign up
 * @author Torti Ama-Njoku @ UXP
 */
public class SignUpDataHolderRequest {

    @NotNull
    private String username;

    @NotNull
    private String password;

    @NotNull
    private String passwordRetype;
    
    /**
     * Default Constructor
     */
    public SignUpDataHolderRequest() {
        super();
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the passwordRetype
     */
    public String getPasswordRetype() {
        return passwordRetype;
    }

    /**
     * @param passwordRetype the passwordRetype to set
     */
    public void setPasswordRetype(String passwordRetype) {
        this.passwordRetype = passwordRetype;
    }
}
