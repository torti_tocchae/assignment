package com.uxpsystems.assignment.exception;

import com.uxpsystems.assignment.enums.ResponseEnum;

/**
 * Exception class to define exception for when a parameter does not pass validation.
 *
 * @author Torti Ama-Njoku @ UXP
 */
@SuppressWarnings("serial")
public class InvalidParameterException extends AssignmentException {

    /**
     * Default Constructor
     */
    public InvalidParameterException() { }

    /**
     * Constructor to with exception message
     * @param message exception message
     * @param exceptionResponse the response object associated with this exception
     */
    public InvalidParameterException(String message, ResponseEnum exceptionResponse) {
        super(message, exceptionResponse);
    }

    /**
     * Constructor with message and exception cause
     * @param message exception message
     * @param cause exception cause
     * @param exceptionResponse the response object associated with this exception
     */
    public InvalidParameterException(String message, Throwable cause, ResponseEnum exceptionResponse) {
        super(message, cause, exceptionResponse);
    }
}
