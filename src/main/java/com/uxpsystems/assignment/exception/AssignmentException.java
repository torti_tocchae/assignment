package com.uxpsystems.assignment.exception;

import com.uxpsystems.assignment.enums.ResponseEnum;

/**
 * Exception super class.
 *
 * @author Torti Ama-Njoku @ UXP
 */
@SuppressWarnings("serial")
public class AssignmentException extends Exception {
    
    private ResponseEnum exceptionResponse;
    
    /**
     * Default Constructor
     */
    public AssignmentException() { }

    /**
     * Constructor to with exception message
     * @param message exception message
     * @param exceptionResponse the response object associated with this exception
     */
    public AssignmentException(String message, ResponseEnum exceptionResponse) {
        super(message);
        this.exceptionResponse = exceptionResponse;
    }

    /**
     * Constructor with message and exception cause
     * @param message exception message
     * @param cause exception cause
     * @param exceptionResponse the response object associated with this exception
     */
    public AssignmentException(String message, Throwable cause, ResponseEnum exceptionResponse) {
        super(message, cause);
        this.exceptionResponse = exceptionResponse;
    }

    /**
     * @return the exceptionResponse
     */
    public ResponseEnum getExceptionResponse() {
        return exceptionResponse;
    }

    /**
     * @param exceptionResponse the exceptionResponse to set
     */
    public void setExceptionResponse(ResponseEnum exceptionResponse) {
        this.exceptionResponse = exceptionResponse;
    }
}