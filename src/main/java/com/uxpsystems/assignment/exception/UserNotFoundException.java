package com.uxpsystems.assignment.exception;

import com.uxpsystems.assignment.enums.ResponseEnum;

/**
 * Exception class to define exception for when an object cannot be found in the database.
 *
 * @author Torti Ama-Njoku @ UXP
 */
@SuppressWarnings("serial")
public class UserNotFoundException extends AssignmentException {

    /**
     * Default Constructor
     */
    public UserNotFoundException() { }

    /**
     * Constructor to with exception message
     * @param message exception message
     * @param exceptionResponse the response object associated with this exception
     */
    public UserNotFoundException(String message, ResponseEnum exceptionResponse) {
        super(message, exceptionResponse);
    }

    /**
     * Constructor with message and exception cause
     * @param message exception message
     * @param cause exception cause
     * @param exceptionResponse the response object associated with this exception
     */
    public UserNotFoundException(String message, Throwable cause, ResponseEnum exceptionResponse) {
        super(message, cause, exceptionResponse);
    }
}
