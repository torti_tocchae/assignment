package com.uxpsystems.assignment.exception;

import com.uxpsystems.assignment.enums.ResponseEnum;

/**
 * Exception class to define exception for when mandatory details is missing 
 * from user entity usually thrown on creation or update.
 *
 * @author Torti Ama-Njoku @ UXP
 */
@SuppressWarnings("serial")
public class MandatoryDetailsException extends AssignmentException {

    /**
     * Default Constructor
     */
    public MandatoryDetailsException() { }

    /**
     * Constructor to with exception message
     * @param message exception message
     * @param exceptionResponse the response object associated with this exception
     */
    public MandatoryDetailsException(String message, ResponseEnum exceptionResponse) {
        super(message, exceptionResponse);
    }

    /**
     * Constructor with message and exception cause
     * @param message exception message
     * @param cause exception cause
     * @param exceptionResponse the response object associated with this exception
     */
    public MandatoryDetailsException(String message, Throwable cause, ResponseEnum exceptionResponse) {
        super(message, cause, exceptionResponse);
    }
}
