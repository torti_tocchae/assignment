package com.uxpsystems.assignment.dao;

import com.uxpsystems.assignment.data.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * This is the data access interface used for retrieving and searching the User
 * entity table for access to the User data.
 * 
 * @author Torti Ama-Njoku @ UXP
 */
public interface UserDao extends JpaRepository<User, Long>, JpaSpecificationExecutor<User> { }
