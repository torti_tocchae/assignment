/**
 * This package contains search specification classes, used to define search conditions
 * for retrieving object data.
 * 
 * @since 1.0
 * @author Torti Ama-Njoku @ UXP
 * @version 1.0
 */
package com.uxpsystems.assignment.dao.specification;
