package com.uxpsystems.assignment.dao.specification;

import com.uxpsystems.assignment.data.model.User_;
import com.uxpsystems.assignment.data.model.User;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;

/**
 * A specification builder class which is used to build JPA specifications for
 * retrieving user data.
 *
 * @author Torti Ama-Njoku @ UXP
 */
public final class UserSpecifications {
    
    /**
     * Build the query to retrieve a user by their username
     * @param username username used to search for the user
     * @return the JPA specification for this search
     */
    public static Specification<User> searchByUsername(String username) {
        return (Root<User> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) -> 
                criteriaBuilder.equal(root.<String>get(User_.username), username);
    }
}
