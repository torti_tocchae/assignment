package com.uxpsystems.assignment.controller;

import com.uxpsystems.assignment.data.model.User;
import com.uxpsystems.assignment.enums.ResponseEnum;
import com.uxpsystems.assignment.exception.AlreadyExistsException;
import com.uxpsystems.assignment.exception.InvalidParameterException;
import com.uxpsystems.assignment.exception.MandatoryDetailsException;
import com.uxpsystems.assignment.exception.UserNotFoundException;
import com.uxpsystems.assignment.rest.request.SignUpDataHolderRequest;
import com.uxpsystems.assignment.rest.request.UpdateUserDataHolderRequest;
import com.uxpsystems.assignment.rest.response.GenericWSDataHolderResponse;
import com.uxpsystems.assignment.rest.response.UserDataHolderResponse;
import com.uxpsystems.assignment.rest.response.UserListDataHolderResponse;
import java.util.Date;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.uxpsystems.assignment.service.UserService;
import java.util.List;
import java.util.Locale;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * API Class that holds all the webservices for user operations
 * @author Torti Ama-Njoku @ UXP
 */
@RestController
@RequestMapping("/user")
@Transactional
@Validated
public class UserApiController {

    private final UserService userService;

    private final MessageSource messageSource;
    
    private final Locale locale = Locale.CANADA;
    
    /**
     * Spring will autowire the parameters of this method by argument
     * resolvers the parameters in this constructor.
     * 
     * @param userService user service singleton used to operate on the user data
     * @param messageSource source for configured messages
     */
    public UserApiController(final UserService userService, final MessageSource messageSource) {
        this.userService = userService;
        this.messageSource = messageSource;
    }

    // Define the logger object for this class
    private static final Logger LOGGER = LoggerFactory.getLogger(UserApiController.class);
    
    /**
     * Webservice API called when a user wants to sign up
     * @param dataHolderRequest {@link SignUpDataHolderRequest} webservice rest input object
     * @return {@link ResponseEntity} HTTP response entity with JSON response attributes
     * @throws AlreadyExistsException
     * @throws MandatoryDetailsException
     * @throws InvalidParameterException
     * @throws UserNotFoundException 
     */
    @RequestMapping(value = "/signUp", method = RequestMethod.POST)
    public ResponseEntity<UserDataHolderResponse> signUp(
            @Valid @RequestBody SignUpDataHolderRequest dataHolderRequest) 
            throws AlreadyExistsException, MandatoryDetailsException, 
            InvalidParameterException, UserNotFoundException {

        LOGGER.debug("Calling signUp");
        
        // Calling the sign up method
        User signedUpUser = userService.signUp(dataHolderRequest.getUsername(), 
                dataHolderRequest.getPassword(), dataHolderRequest.getPasswordRetype());
        
        // Build response object
        UserDataHolderResponse response = new UserDataHolderResponse(ResponseEnum.SUCCESSFUL.getCode(),
                messageSource.getMessage(ResponseEnum.SUCCESSFUL.getLabelKey(),
                        new Object[]{messageSource.getMessage("user.signup.successful", null, locale)},
                        locale),
                ResponseEnum.SUCCESSFUL.getHttpCode().value(), new Date().getTime(), signedUpUser);

        return new ResponseEntity<>(response, ResponseEnum.SUCCESSFUL.getHttpCode());
    }
    
    /**
     * Webservice API called to get a list of all users in the system
     * @return {@link ResponseEntity} HTTP response entity with JSON response attributes
     * @throws AuthenticationFailedException
     * @throws InactiveUserException 
     */
    @RequestMapping(value = "/getUsers", method = RequestMethod.GET)
    public ResponseEntity<UserListDataHolderResponse> getUsers() {

        LOGGER.debug("Calling getUsers");

        // Calling the sign in method
        List<User> users = userService.getAllUsers();
        
        // Build response object
        UserListDataHolderResponse response = new UserListDataHolderResponse(ResponseEnum.SUCCESSFUL.getCode(),
                messageSource.getMessage(ResponseEnum.SUCCESSFUL.getLabelKey(),
                        new Object[]{messageSource.getMessage("user.getAllUsers.successful", null, locale)},
                        locale),
                ResponseEnum.SUCCESSFUL.getHttpCode().value(), new Date().getTime(), users);

        return new ResponseEntity<>(response, ResponseEnum.SUCCESSFUL.getHttpCode());
    }

    /**
     * Webservice API called when a user wants to update their profile
     * @param userId id of the user to be updated or created if it does not exist
     * @param dataHolderRequest {@link UpdateUserDataHolderRequest} webservice rest input object
     * @return {@link ResponseEntity} HTTP response entity with JSON response attributes
     * @throws AlreadyExistsException 
     * @throws MandatoryDetailsException 
     */
    @RequestMapping(value = "/updateUser/{userId}", method = RequestMethod.PUT)
    public ResponseEntity<UserDataHolderResponse> updateUser(
            @PathVariable Long userId,
            @Valid @RequestBody UpdateUserDataHolderRequest dataHolderRequest) 
            throws AlreadyExistsException, MandatoryDetailsException {

        LOGGER.debug("Calling user update");
        
        ResponseEnum responseCode = ResponseEnum.SUCCESSFUL;
        
        // Get the user updating the profile
        User userToUpdate;
        User updatedUser;
        try {
            userToUpdate = userService.findById(userId);
            
            // update user
            updatedUser = userService.updateUser(userToUpdate, dataHolderRequest.getUsername(), 
                    dataHolderRequest.getNewPassword(), dataHolderRequest.getStatus());
            
        } catch (UserNotFoundException ex) {
            userToUpdate = new User(userId, dataHolderRequest.getStatus(),
                    dataHolderRequest.getUsername(), dataHolderRequest.getNewPassword());
            
            // Save the new user
            updatedUser = userService.saveUser(userToUpdate);
            
            responseCode = ResponseEnum.CONTENT_CREATED;
        }

        // Build response object
        UserDataHolderResponse response = new UserDataHolderResponse(responseCode.getCode(),
                messageSource.getMessage(ResponseEnum.SUCCESSFUL.getLabelKey(),
                        new Object[]{messageSource.getMessage("user.updateUser.successful", null, locale)},
                        locale),
                responseCode.getHttpCode().value(), new Date().getTime(), updatedUser);

        return new ResponseEntity<>(response, responseCode.getHttpCode());
    }
    
    /**
     * Webservice API called when a user is to be deleted
     * @param userId id of the user to be deleted
     * @return {@link ResponseEntity} HTTP response entity with JSON response attributes
     * @throws UserNotFoundException
     */
    @RequestMapping(value = "/deleteUser/{userId}", method = RequestMethod.DELETE)
    public ResponseEntity<GenericWSDataHolderResponse> deleteUser(
            @PathVariable Long userId) throws UserNotFoundException {

        LOGGER.debug("Calling delete user with id: {0}", userId);

        // Call the delete method
        userService.deleteUser(userId);
        
        // Build response object
        GenericWSDataHolderResponse response = new GenericWSDataHolderResponse(ResponseEnum.SUCCESSFUL.getCode(),
                messageSource.getMessage(ResponseEnum.SUCCESSFUL.getLabelKey(),
                        new Object[]{messageSource.getMessage("user.deleteUser.successful", null, locale)},
                        locale),
                ResponseEnum.SUCCESSFUL.getHttpCode().value(), new Date().getTime());

        return new ResponseEntity<>(response, ResponseEnum.SUCCESSFUL.getHttpCode());
    }
}
