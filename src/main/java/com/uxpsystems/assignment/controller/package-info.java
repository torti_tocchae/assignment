/**
 * This package contains API class definitions for webservice endpoints.
 * 
 * @since 1.0
 * @author Torti Ama-Njoku @ Tocchae
 * @version 1.0
 */
package com.uxpsystems.assignment.controller;
