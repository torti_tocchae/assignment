package com.uxpsystems.assignment.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

/**
 * This class defines the Security configuration for the application
 * @author Torti Ama-Njoku @ UXP
 */
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
          .csrf().disable()
          .authorizeRequests()
            .antMatchers("/user/**").authenticated()
            .anyRequest().permitAll()
            .and()
          .httpBasic().and()
          .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
  }
}
