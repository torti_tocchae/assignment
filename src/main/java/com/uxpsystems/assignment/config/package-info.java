/**
 * This package contains all spring boot configuration files.
 * 
 * @since 1.0
 * @author Torti Ama-Njoku @ UXP
 * @version 1.0
 */
package com.uxpsystems.assignment.config;
