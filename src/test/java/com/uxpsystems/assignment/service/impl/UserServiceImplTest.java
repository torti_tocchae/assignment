package com.uxpsystems.assignment.service.impl;

import com.uxpsystems.assignment.service.impl.UserServiceImpl;
import com.uxpsystems.assignment.dao.UserDao;
import com.uxpsystems.assignment.data.model.User;
import com.uxpsystems.assignment.enums.UserStatusEnum;
import com.uxpsystems.assignment.exception.AlreadyExistsException;
import com.uxpsystems.assignment.exception.InvalidParameterException;
import com.uxpsystems.assignment.exception.MandatoryDetailsException;
import com.uxpsystems.assignment.exception.UserNotFoundException;
import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import org.junit.Rule;
import org.junit.rules.ExpectedException;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.springframework.context.MessageSource;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * UserService Test 
 * @author Torti Ama-Njoku @ UXP
 */
public class UserServiceImplTest {
    
    private PasswordEncoder passwordEncoder;
    private UserDao userDao;
    private MessageSource messageSource;
    private UserServiceImpl userService;
    
    private final String genPassword = "$2y$10$reY4k7WA6Qr1IlyT.xhQYOTy9R3vWMZWotlDqudQ9svxaSqQHqfqq";
    private final String genPassword2 = "$2y$10$3sce8MKu.J.saeCtCJEtS.H23xkeDaFVt6ZZvnTNmo.xINIHVFKwa";
    
    @Rule
    public final ExpectedException exception = ExpectedException.none();
    
    @Before
    public void setUp() {
        // Mock all the singleton beans
        passwordEncoder = mock(PasswordEncoder.class);
        userDao = mock(UserDao.class);
        messageSource = mock(MessageSource.class);
        
        // Create an instance of the user service
        userService = new UserServiceImpl(passwordEncoder, userDao, messageSource);
    }

    /**
     * Test of getAllUsers method, of class UserServiceImpl.
     */
    @Test
    public void testGetAllUsers() {
        // Test for data being returned
        when(userDao.findAll()).thenReturn(getListOfUsers());
        
        List<User> users = userService.getAllUsers();
        
        assertEquals(users.size(), 4);
        assertEquals(users.get(0).getId().longValue(), 1L);
        
        // Test for no data being returned
        when(userDao.findAll()).thenReturn(null);
        users = userService.getAllUsers();
        
        assertEquals(users, null);
    }

    /**
     * Test of findById method, of class UserServiceImpl.
     */
    @Test
    public void testFindById() throws UserNotFoundException {
        // Test for when an ID exists
        Long id = 1L;
        when(userDao.findOne(id)).thenReturn(getListOfUsers().get(0));
        
        User user = userService.findById(id);
        
        assertEquals(user.getUsername(), "testUser1");
        
        // Test for when id does not exist
        id = 5L;
        when(userDao.findOne(id)).thenReturn(null);
        exception.expect(UserNotFoundException.class);
        
        userService.findById(id);
    }

    /**
     * Test of findUser method, of class UserServiceImpl.
     */
    @Test
    public void testFindUser() throws UserNotFoundException {
        // Test for when an username exists
        String username = "testUser2";
        when(userDao.findOne(any(Specification.class))).thenReturn(getListOfUsers().get(1));
        
        User user = userService.findUser(username);
        
        assertEquals(user.getId().longValue(), 2L);
        
        // Test for when username does not exist
        username = "testUser5";
        when(userDao.findOne(any(Specification.class))).thenReturn(null);
        exception.expect(UserNotFoundException.class);
        
        userService.findUser(username);
    }

    /**
     * Test of deleteUser method, of class UserServiceImpl.
     */
    @Test
    public void testDeleteUser() throws Exception {
        // Test for when id does not exist
        Long id = 5L;
        when(userDao.findOne(id)).thenReturn(null);
        exception.expect(UserNotFoundException.class);
        
        userService.deleteUser(id);
    }

    /**
     * Test of saveUser method, of class UserServiceImpl.
     */
    @Test
    public void testSaveUser() throws Exception {
        // Test for when password is missing
        User user = new User("testUser5", null);
        exception.expect(MandatoryDetailsException.class);
        
        userService.saveUser(user);
        
        // Test for when username is blank
        user = new User(null, "Password1");
        exception.expect(MandatoryDetailsException.class);
        
        userService.saveUser(user);
        
        // Test for when staus is null
        user = new User(5L, null, "testUser5", genPassword);
        exception.expect(MandatoryDetailsException.class);
        
        userService.saveUser(user);
        
        // Test when saving a new user, but for a username that exists
        user = new User("testUser4", "Password1");
        when(userDao.findOne(any(Specification.class))).thenReturn(getListOfUsers().get(3));
        exception.expect(AlreadyExistsException.class);
        
        userService.saveUser(user);
        
        // Test for a successful save
        user = new User("testUser5", "Password1");
        when(passwordEncoder.encode("Password1")).thenReturn(genPassword);
        when(userDao.saveAndFlush(user)).thenReturn(
                new User(5L, UserStatusEnum.ACTIVATED, "testUser5", genPassword));
        
        User savedUser = userService.saveUser(user);
        
        assertEquals(savedUser.getId().longValue(), 5L);
        assertEquals(savedUser.getUsername(), "testUser5");
        assertEquals(savedUser.getPassword(), genPassword);
        assertEquals(savedUser.getPassword(), UserStatusEnum.ACTIVATED);
    }
    
    /**
     * Test of updateUser method, of class UserServiceImpl.
     */
    @Test
    public void testUpdateUser() throws Exception {
        // Test when updating an existing user with a username that already exists
        User user = new User(3L, UserStatusEnum.ACTIVATED, "testUser3", genPassword);
        when(userDao.findOne(any(Specification.class))).thenReturn(getListOfUsers().get(1));
        exception.expect(AlreadyExistsException.class);
        
        userService.updateUser(user, "testUser2", null, null);
        
        // Test for a successful update of user details
        user = new User(3L, UserStatusEnum.DEACTIVATED, "testUser3", genPassword);
        when(passwordEncoder.encode("Password0")).thenReturn(genPassword2);
        when(userDao.findOne(any(Specification.class))).thenReturn(null);
        when(userDao.saveAndFlush(user)).thenReturn(
                new User(3L, UserStatusEnum.DEACTIVATED, "testUser5", genPassword2));
        
        User savedUser = userService.updateUser(user, "testUser5", "Password0", UserStatusEnum.DEACTIVATED);
        
        assertEquals(savedUser.getId().longValue(), 3L);
        assertEquals(savedUser.getUsername(), "testUser5");
        assertEquals(savedUser.getPassword(), genPassword2);
        assertEquals(savedUser.getStatus(), UserStatusEnum.DEACTIVATED);
    }

    /**
     * Test of signUp method, of class UserServiceImpl.
     */
    @Test
    public void testSignUp() throws Exception {
        
        // Test password and confirm password mismatch
        exception.expect(InvalidParameterException.class);
        userService.signUp("testUser5", "Password1", "Password0");
        
        // Test for when password is missing
        exception.expect(MandatoryDetailsException.class);
        userService.signUp("testUser5", " ", " ");
        
        // Test for when username is blank
        exception.expect(MandatoryDetailsException.class);
        userService.signUp("", "Password1", "Password1");
        
        // Test when saving a new user, but for a username that exists
        when(userDao.findOne(any(Specification.class))).thenReturn(getListOfUsers().get(3));
        userService.signUp("testUser4", "Password1", "Password1");
        
        // Test for a successful sign up
        User user = new User("testUser5", "Password1");
        when(passwordEncoder.encode("Password1")).thenReturn(genPassword);
        when(userDao.saveAndFlush(user)).thenReturn(
                new User(5L, UserStatusEnum.ACTIVATED, "testUser5", genPassword));
        
        User savedUser = userService.signUp("testUser5", "Password1", "Password1");
        
        assertEquals(savedUser.getId().longValue(), 5L);
        assertEquals(savedUser.getUsername(), "testUser5");
        assertEquals(savedUser.getPassword(), genPassword);
        assertEquals(savedUser.getPassword(), UserStatusEnum.ACTIVATED);
    }
    
    /**
     * Get a list of sample users
     * @return List of users
     */
    private List<User> getListOfUsers() {
        List<User> users = new ArrayList<>();
        
        users.add(new User(1L, UserStatusEnum.ACTIVATED, "testUser1", genPassword));
        users.add(new User(2L, UserStatusEnum.ACTIVATED, "testUser2", genPassword));
        users.add(new User(3L, UserStatusEnum.ACTIVATED, "testUser3", genPassword));
        users.add(new User(4L, UserStatusEnum.ACTIVATED, "testUser4", genPassword));
        
        return users;
    }
    
}
