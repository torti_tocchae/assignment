package com.uxpsystems.assignment.controller;

import com.uxpsystems.assignment.UXPSystemsAssignmentApplication;
import com.uxpsystems.assignment.dao.UserDao;
import com.uxpsystems.assignment.data.model.User;
import com.uxpsystems.assignment.enums.UserStatusEnum;
import com.uxpsystems.assignment.rest.request.SignUpDataHolderRequest;
import com.uxpsystems.assignment.rest.request.UpdateUserDataHolderRequest;
import com.uxpsystems.assignment.rest.response.GenericWSDataHolderResponse;
import com.uxpsystems.assignment.rest.response.UserDataHolderResponse;
import com.uxpsystems.assignment.rest.response.UserListDataHolderResponse;
import com.uxpsystems.assignment.service.UserService;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * UserApiController Test 
 * @author Torti Ama-Njoku @ UXP
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = UXPSystemsAssignmentApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserApiControllerTest {
    
    @LocalServerPort
    private int port;

    private TestRestTemplate restTemplate;

    private HttpHeaders headers;
    
    @Autowired
    UserService userService;
    
    @Autowired
    UserDao userDao;
    
    @Autowired
    PasswordEncoder passwordEncoder;
    
    @Before
    public void setUp() throws Exception {
        restTemplate = new TestRestTemplate();
        headers = new HttpHeaders();
        
        setupAuthorization();
    }
    
    /**
     * Test of signUp method, of class UserApiController.
     */
    @Test
    public void testSignUp() throws Exception {
        
        // Setup the DB with data
        saveTestUsers();
        
        // Test password and confirm password mismatch
        SignUpDataHolderRequest dataHolderRequest = new SignUpDataHolderRequest();
        dataHolderRequest.setUsername("testUser5");
        dataHolderRequest.setPassword("Password1");
        dataHolderRequest.setPasswordRetype("Password0");
        
        UserDataHolderResponse response = callSignUpEndpoint(dataHolderRequest);
        
        assertEquals(400, response.getStatus()); // Should return HTTP Status BAD_REQUEST
        assertEquals("114", response.getResponseCode());
        
        // Test for when password is missing
        dataHolderRequest.setUsername("testUser5");
        dataHolderRequest.setPassword(" ");
        dataHolderRequest.setPasswordRetype(" ");
        
        response = callSignUpEndpoint(dataHolderRequest);
        
        assertEquals(206, response.getStatus()); // Should return HTTP Status PARTIAL_CONTENT
        assertEquals("112", response.getResponseCode());
        
        // Test for when username is blank
        dataHolderRequest.setUsername("");
        dataHolderRequest.setPassword("Password1");
        dataHolderRequest.setPasswordRetype("Password1");
        
        response = callSignUpEndpoint(dataHolderRequest);
        
        assertEquals(206, response.getStatus()); // Should return HTTP Status PARTIAL_CONTENT
        assertEquals("112", response.getResponseCode());
        
        // Test when saving a new user, but for a username that exists
        dataHolderRequest.setUsername("testUser1");
        dataHolderRequest.setPassword("Password1");
        dataHolderRequest.setPasswordRetype("Password1");
        
        response = callSignUpEndpoint(dataHolderRequest);
        
        assertEquals(409, response.getStatus()); // Should return HTTP Status CONFLICT
        assertEquals("113", response.getResponseCode());
        
        // Test a successful creation
        dataHolderRequest.setUsername("testUser4");
        dataHolderRequest.setPassword("Password1");
        dataHolderRequest.setPasswordRetype("Password1");
        
        response = callSignUpEndpoint(dataHolderRequest);
        
        assertEquals(200, response.getStatus()); // Should return HTTP Status CONFLICT
        assertEquals("00", response.getResponseCode());
        assertEquals(true, response.getUser().isPersisted());
        
        // Clear the DB
        userDao.deleteAll();
        userDao.flush();
        
    }

    /**
     * Test of getUsers method, of class UserApiController.
     */
    @Test
    public void testGetUsers() throws Exception {
        // Setup the DB with users
        saveTestUsers();
        
        // Test for when there is data
        UserListDataHolderResponse response = callGetUsersEndpoint();
        
        assertEquals(200, response.getStatus()); // Should return HTTP Status CONFLICT
        assertEquals("00", response.getResponseCode());
        assertEquals(3, response.getUsers().size(), 3);
        assertEquals(true, response.getUsers().contains(
                new User(2L, UserStatusEnum.ACTIVATED, "testUser2", "$2y$10$reY4k7WA6Qr1IlyT.xhQYOTy9R3vWMZWotlDqudQ9svxaSqQHqfqq")));
        
        // Clear the DB
        userDao.deleteAll();
        userDao.flush();
    }

    /**
     * Test of updateUser method, of class UserApiController.
     */
    @Test
    public void testUpdateUser() throws Exception {
        // Setup the DB with data
        saveTestUsers();
        
        // Get a user to update
        User userToUpdate = userService.findUser("testUser1");
        
        // Test update a user with a new username that is assigned to another user
        UpdateUserDataHolderRequest dataHolderRequest = new UpdateUserDataHolderRequest();
        dataHolderRequest.setUsername("testUser2");
        
        UserDataHolderResponse response = callUpdateUserEndpoint(userToUpdate.getId(), dataHolderRequest);
        
        assertEquals(409, response.getStatus()); // Should return HTTP Status CONFLICT
        assertEquals("113", response.getResponseCode());
        
        // Test update a user with a new details successfully
        dataHolderRequest.setUsername("testUser4");
        dataHolderRequest.setNewPassword("Password0");
        dataHolderRequest.setStatus(UserStatusEnum.DEACTIVATED);
        
        response = callUpdateUserEndpoint(userToUpdate.getId(), dataHolderRequest);
        
        assertEquals(200, response.getStatus()); // Should return HTTP Status OK
        assertEquals("00", response.getResponseCode());
        
        // Get the new user from the DB
        User newUser = userService.findUser("testUser4");
        
        assertEquals(UserStatusEnum.DEACTIVATED, newUser.getStatus());
        assertEquals(true, passwordEncoder.matches("Password0", newUser.getPassword()));
        
        // Test update a user that does not exist and create a new record
        dataHolderRequest.setUsername("testUser5");
        dataHolderRequest.setNewPassword("Password0");
        dataHolderRequest.setStatus(UserStatusEnum.ACTIVATED);
        
        response = callUpdateUserEndpoint(20L, dataHolderRequest);
        
        assertEquals(201, response.getStatus()); // Should return HTTP Status OK
        assertEquals("00", response.getResponseCode());
        
        // Get the new user from the DB
        newUser = userService.findUser("testUser5");
        
        assertEquals(true, newUser.isPersisted());
        assertEquals(UserStatusEnum.ACTIVATED, newUser.getStatus());
        assertEquals(true, passwordEncoder.matches("Password0", newUser.getPassword()));
        
        // Clear the DB
        userDao.deleteAll();
        userDao.flush();
    }

    /**
     * Test of deleteUser method, of class UserApiController.
     */
    @Test
    public void testDeleteUser() throws Exception {
        // Setup the DB with data
        saveTestUsers();
        
        // Get a user to update
        User userToDelete = userService.findUser("testUser1");
        
        // Test Deleteing a user that does not exist
        ResponseEntity<GenericWSDataHolderResponse> response = callDeleteUserEndpoint(1555L);
        
        assertEquals(204, response.getStatusCodeValue()); // Should return HTTP Status NO_CONTENT
        
        // Test a successful delete
        response = callDeleteUserEndpoint(userToDelete.getId());
        GenericWSDataHolderResponse responseBody = response.getBody();
        
        assertEquals(200, responseBody.getStatus()); // Should return HTTP Status OK
        assertEquals("00", responseBody.getResponseCode()); // Should return HTTP Status OK
        
        // Clear the DB
        userDao.deleteAll();
        userDao.flush();
    }
    
    /**
     * Save a set of test users
     */
    private void saveTestUsers() throws Exception {
        userService.saveUser(new User("testUser1", "$2y$10$reY4k7WA6Qr1IlyT.xhQYOTy9R3vWMZWotlDqudQ9svxaSqQHqfqq"));
        userService.saveUser(new User("testUser2", "$2y$10$reY4k7WA6Qr1IlyT.xhQYOTy9R3vWMZWotlDqudQ9svxaSqQHqfqq"));
        userService.saveUser(new User("testUser3", "$2y$10$reY4k7WA6Qr1IlyT.xhQYOTy9R3vWMZWotlDqudQ9svxaSqQHqfqq"));
    }
    
    private UserDataHolderResponse callSignUpEndpoint(SignUpDataHolderRequest dataHolderRequest) throws Exception{
        HttpEntity<SignUpDataHolderRequest> entity = new HttpEntity<>(dataHolderRequest, headers);
        ResponseEntity<UserDataHolderResponse> response = restTemplate.exchange(
                        createURLWithPort("/assignment/user/signUp"),
                        HttpMethod.POST, entity, UserDataHolderResponse.class);
        
        return response.getBody();
    }
    
    private UserListDataHolderResponse callGetUsersEndpoint() throws Exception{
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<UserListDataHolderResponse> response = restTemplate.exchange(
                        createURLWithPort("/assignment/user/getUsers"),
                        HttpMethod.GET, entity, UserListDataHolderResponse.class);
        
        return response.getBody();
    }
    
    private UserDataHolderResponse callUpdateUserEndpoint(Long userId, UpdateUserDataHolderRequest dataHolderRequest) throws Exception{
        HttpEntity<UpdateUserDataHolderRequest> entity = new HttpEntity<>(dataHolderRequest, headers);
        ResponseEntity<UserDataHolderResponse> response = restTemplate.exchange(
                        createURLWithPort("/assignment/user/updateUser/" + userId),
                        HttpMethod.PUT, entity, UserDataHolderResponse.class);
        
        return response.getBody();
    }
    
    private ResponseEntity<GenericWSDataHolderResponse> callDeleteUserEndpoint(Long userId) throws Exception{
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<GenericWSDataHolderResponse> response = restTemplate.exchange(
                        createURLWithPort("/assignment/user/deleteUser/" + userId),
                        HttpMethod.DELETE, entity, GenericWSDataHolderResponse.class);
        
        return response;
    }
    
    private String createURLWithPort(String uri) {
        return "http://localhost:" + port + uri;
    }
    
    private void setupAuthorization() {
        // Add Authentication Header
        String plainCredentials = "uxpsystems:uxppassword";
        String base64Credentials = new String(Base64.encode(plainCredentials.getBytes()));
         
        headers.add("Authorization", "Basic " + base64Credentials);
    }
}
