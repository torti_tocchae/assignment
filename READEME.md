This is the assignment project from UXP. 
This is a basic users management microservice built with sring-rest and spring-jpa
with an in memory HSQLDB Database. On reboot of the application, the database will be
cleaned out.

This microservice implements 3 endpoints:

1. getUsers - Gives you a list of all the users in the system
2. signUp - allows you to register a user
3. updateUser - allows you to update an already existing user, but if the requested user does not exist, it will create one.
4. deleteUser - allows you to delete an already existing user

In order to build and run this project you must ensure you have the following installed and setup:

1. Maven - at least version 3.3
2. Java - at least version 1.8 (Has not been tested with 1.9)

To compile project please run the following from the root directory of the project:
    mvn clean install

The project is built with spring boot, and has an embedded tomcat server, so in order to run the project,
please execute the following command:
    mvn spring-boot:run

The project can be accessed from this URL:

    http://localhost:9090/assignment --> This will load the swagger UI and allow you a view of all the endpoints

Some Known Issues:

When swagger UI is loaded, it throws an error, because it tries to fill a default string value for one of the endpoint numeric fields. Please ignore this error.
It will not impact running and executing the endpoints. This will be fixed in the next version.

ENJOY!!! 